###
# Implcit euler to solve heat equaiton using Cranck-Nicholson scheme

import numpy as np
from matplotlib import pyplot as plt
import scipy as sp
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve
from matplotlib import animation


kappa=0.01
T=1
mx=50
mt=200
L=1
def u_I(x):
    # initial temperature distribution
    y = np.sin(np.pi*x/L)
    return y

def u_exact(x,t):
    # the exact solution
    y = np.exp(-kappa*(np.pi**2/L**2)*t)*np.sin(np.pi*x/L)
    return y

def cnmatrixa(n,lmbda):
    """Returns the A matrix using CN scheme"""
    a=diags([-lmbda/2,1+lmbda,-lmbda/2],[-1,0,1],shape=(n,n),format = 'csr')
    return a

def cnmatrixb(n,lmbda):
    """Returns the B matrix using CN scheme"""
    a=diags([lmbda/2,1-lmbda,lmbda/2],[-1,0,1],shape=(n,n),format = 'csr')
    return a



def finite_diff_CN(kappa,T,mx,mt,u_I):
    """Finite difference iteration scheme using Cranck-Nicholson"""
    
    x = np.linspace(0, L, mx+1)     
    t = np.linspace(0, T, mt+1)     
    deltax = x[1] - x[0]           
    deltat = t[1] - t[0]            
    lmbda = kappa*deltat/(deltax**2)   
    u_j = np.zeros(x.size) 
    u_jp1 = np.zeros(x.size)   
    
    for i in range(1,mx):
        u_j[i] = u_I(x[i])
     
  

    for n in range(1, mt+1):
        u_jp1[1:mx]=spsolve(cnmatrixa(mx-1,lmbda),cnmatrixb(mx-1,lmbda).dot(u_j[1:mx]))
        u_jp1[0] = 0; u_jp1[mx] = 0
        u_j[:] = u_jp1[:]
        
    return x,u_j,lmbda,deltax,deltat
    
x,u_j,lmbda,dx,dt=finite_diff_CN(kappa,T,mx,mt,u_I)
plt.plot(x,u_j,'ro',label='num')    
xx = np.linspace(0,L,250)
plt.plot(xx,u_exact(xx,T),'b-',label='exact')
plt.xlabel('x')
plt.ylabel('u(x,T)')
plt.legend(loc='upper right')
plt.show()




#Error comparison

errorx=[]
dxx=[]
dmx=np.arange(10,100,10)#changing error in x
for n in dmx:
    x,u_j,lmbda,dx,dt=finite_diff_CN(kappa,T,n,200,u_I)
    u_ex=u_exact(x,T)
    err=sum(np.abs(u_j-u_ex))/n
    errorx.append(err)
    dxx.append(dx)

plt.plot(dxx,errorx)
plt.xlabel("dx")
plt.ylabel("error")
plt.show()


errort=[]
dtt=[]
dmt=np.arange(10,100,10)#changing error in t
for n in dmt:
    x,u_j,lmbda,dx,dt=finite_diff_CN(kappa,T,50,n,u_I)
    u_ex=u_exact(x,T)
    err=sum(np.abs(u_j-u_ex))/n
    errort.append(err)
    dtt.append(dt)

plt.plot(dtt,errort)
plt.xlabel("dt")
plt.ylabel("error")
plt.show()