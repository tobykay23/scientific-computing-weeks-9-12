###
#Solving the heat eqaution using implict euler method, the backwards euler method


import numpy as np
from matplotlib import pyplot as plt
import scipy as sp

kappa = 1   # diffusion constant
L=1.0         # length of spatial domain
T=0.5      # total time to solve for
def u_I(x):
    # initial temperature distribution
    y = np.sin(np.pi*x/L)
    return y

def u_exact(x,t):
    # the exact solution
    y = np.exp(-kappa*(np.pi**2/L**2)*t)*np.sin(np.pi*x/L)
    return y

mx = 10     
mt = 1000   



x = np.linspace(0, L, mx+1)     
t = np.linspace(0, T, mt+1)     
deltax = x[1] - x[0]            
deltat = t[1] - t[0]           
lmbda = kappa*deltat/(deltax**2)    
print("deltax=",deltax)
print("deltat=",deltat)
print("lambda=",lmbda)

u_j = np.zeros(x.size) 
u_jp1 = np.zeros(x.size)   


def backmatrix(n):
    a=np.diag([-lmbda]*(n-1),k=-1)
    b=np.diag([-lmbda]*(n-1),k=1)    
    c=np.diag([1+2*lmbda]*n)
    return a+b+c


    
for i in range(0,mx+1):
    u_j[i] = u_I(x[i])
     
  

for n in range(1, mt+1):
    u_jp1[1:mx]=np.linalg.solve(backmatrix(mx-1),u_j[1:mx])
    u_jp1[0] = 0; u_jp1[mx] = 0
    u_j[:] = u_jp1[:]

plt.plot(x,u_j,'ro',label='num')
xx = np.linspace(0,L,250)
plt.plot(xx,u_exact(xx,T),'b-',label='exact')
plt.xlabel('x')
plt.ylabel('u(x,0.5)')
plt.legend(loc='upper right')
plt.show()
        
    
