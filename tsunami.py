import numpy as np
from matplotlib import pyplot as plt
from scipy.sparse import diags
from matplotlib import animation

# Set problem parameters/functions
c=1.0        # wavespeed
L=15    # length of spatial domain
T=0.1         # total time to solve for
alphaI=1.5
alphaB=0.25
xI=0.15*L #Starting position of wave 
xB=0.8*L #Position of sea floor hill
sigI=0.5
sigB=0.25
h0=2


def u_I(x):
    # initial displacement
    y=alphaI*np.exp(-((x-xI)**2)/sigI**2)
    return y

def v_I(x):
    # initial velocity
    y = 0*x
    return y


def h(x):
    """Gaussian function to represent the still water height"""
    return h0+alphaB*np.exp(-((x-xB)**2)/sigB**2)
    
def variable_AFE(x,delta,deltax,n):
    """Function to return the forward euler matrix for variable wave speed"""
    a=np.zeros(n-1)
    b=np.zeros(n)
    c=np.zeros(n-1)
    for i in range(0,n-1):
        
        a[i]=(delta**2)*(h(x[i]-0.5*deltax))
        
        c[i]=(delta**2)*(h(x[i]+0.5*deltax))
    for i in range(0,n):
        b[i]=(2-(delta**2)*(h(x[i]+0.5*deltax) + h(x[i]-0.5*deltax)))
    y=diags([a,b,c],[-1,0,1],shape=(n,n)).toarray()
    
    return y

mx=100
mt=200 
    
def tsunami_wave(c,L,T,mx,mt):
    
    # Set up the numerical environment variables
    x = np.linspace(0, L, mx+1)     # gridpoints in space
    t = np.linspace(0, T, mt+1)     # gridpoints in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = c*deltat/deltax         # squared courant number
    #print("lambda=",lmbda)
    delta = deltat/deltax
    u_jm1 = np.zeros(x.size)        # u at previous time step
    u_j = np.zeros(x.size)          # u at current time step
    u_jp1 = np.zeros(x.size)        # u at next time step
    
    # Set initial condition
    while T<5: #Condition to allow left travelling wave leave doamin
        for i in range(0, mx+1):
            u_jm1[i] = u_I(x[i])  
    
        for i in range(0,mx+1):
            u_j=0.5*variable_AFE(x,delta,deltax,mx+1)@u_jm1 + deltat*v_I(x)
            # Setting open BC as lmbda= h(x)*delta
            u_j[0]=(1-h(x[0])*delta)*u_jm1[0] +h(x[1])*delta*u_jm1[1] ; u_j[mx]=(1-h(x[mx])*delta)*u_jm1[mx] +h(x[mx-1])*delta*u_jm1[mx-1]
            
        for n in range(2,mt+1):
            u_jp1=variable_AFE(x,delta,deltax,mx+1)@u_j - u_jm1
            u_jp1[0]=(1-h(x[0])*delta)*u_j[0] +h(x[1])*delta*u_j[1] ; u_jp1[mx]=(1-h(x[mx])*delta)*u_j[mx] +h(x[mx-1])*delta*u_j[mx-1]
            
            u_jm1[:],u_j[:] = u_j[:],u_jp1[:]
        
        return x,u_jp1,lmbda
    else: #For after the left wave has left can use periodic BCs
        for i in range(0, mx+1):
            u_jm1[i] = u_I(x[i])  
            
        for i in range(0,mx+1):
            u_j=0.5*variable_AFE(x,delta,deltax,mx+1)@u_jm1 + deltat*v_I(x)
            u_j[0]=(1-h(x[0])*delta)*u_jm1[0] +h(x[1])*delta*u_jm1[1] ; u_j[mx]=(1-h(x[mx])*delta)*u_jm1[mx] +h(x[mx-1])*delta*u_jm1[mx-1]
            u_j[0]=u_j[mx] #Periodic BCs
        for n in range(2,mt+1):
            u_jp1=variable_AFE(x,delta,deltax,mx+1)@u_j - u_jm1
            u_jp1[0]=(1-h(x[0])*delta)*u_j[0] +h(x[1])*delta*u_j[1] ; u_jp1[mx]=(1-h(x[mx])*delta)*u_j[mx] +h(x[mx-1])*delta*u_j[mx-1]
            u_jp1[0]=u_jp1[mx]
            u_jm1[:],u_j[:] = u_j[:],u_jp1[:]
        
        return x,u_jp1,lmbda
    
x,u_jp1,lmbda=tsunami_wave(c,L,T,mx,mt)
#plt.plot(x,u_jp1,'r-')
#plt.plot(x,u_I(x))
#plt.plot(x,h(x))
#plt.show()    


# Animation code used to show how the wave changes with time

fig = plt.figure()
ax = plt.axes(xlim=(0, L), ylim=(-5, 5))
line, = ax.plot([], [], lw=2)

def init():
    line.set_data([], [])
    return line,

def animate(i):
    x,u_jp1,lmbda=tsunami_wave(c,L,T+0.2*i,mx,mt)
    line.set_data(x,u_jp1)
    
    return line,
    
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=2000, interval=20, blit=True)

plt.show()