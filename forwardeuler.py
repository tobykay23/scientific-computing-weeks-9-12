###
# Weeks 9 and 10 parabolic PDEs

# Explicit scheme to solve the heat equaiton

import numpy as np
from matplotlib import pyplot as plt
from math import pi
from matplotlib import animation

kappa = 0.01   
L=1         
T=1


def u_I(x):
    # initial temperature distribution
    y = np.sin(np.pi*x/L)
    return y

def u_exact(x,t):
    # the exact solution
    y = np.exp(-kappa*(pi**2/L**2)*t)*np.sin(pi*x/L)
    return y

#Gridspacing
mx = 50     
mt = 200 



def forwardmatrix(n,lmbda):
    """Normal forward euler matrix"""
    a=np.diag([lmbda]*(n-1),k=-1)
    b=np.diag([lmbda]*(n-1),k=1)    
    c=np.diag([1-2*lmbda]*n)
    return a+b+c

def forwardmatrixneumann(n,lmbda):
    """Forward euler matrix adapted for neumann BCs"""
    a=np.diag([lmbda]*(n-1),k=-1)
    b=np.diag([lmbda]*(n-1),k=1)    
    c=np.diag([1-2*lmbda]*n)
    d=a+b+c
    d[0,1]=2*lmbda
    d[n-1,n-2]=2*lmbda
    return d
    

def neumann(n,p,q):
    """The extra vector needed when using neumann BCs"""
    a=np.zeros(n)
    a[0]=-p
    a[n-1]=q
    return a

      

def source(x,t):
    """Function to add a heat source term"""
    return 0
        
    
def finite_diff_forward(kappa,T,mx,mt,u_I,type_of_BC):
    x = np.linspace(0, L, mx+1) #Range of x values
    t = np.linspace(0, T, mt+1) #Range of time values
    deltax = x[1] - x[0]           
    deltat = t[1] - t[0]            
    lmbda = kappa*deltat/(deltax**2)   
    u_j = np.zeros(x.size) 
    u_jp1 = np.zeros(x.size)   
    
    
    if type_of_BC=="dirichlet":
        #Setting ICs
        for i in range(1,mx):
            u_j[i] = u_I(x[i])
       
        for n in range(1, mt+1):
            u_jp1[1:mx]=forwardmatrix(mx-1,lmbda)@u_j[1:mx] + deltat*source(x,t[n])
            u_jp1[0] = 0 ; u_jp1[mx] = 0
            #Nonhomogenous for e.g.
            #u_jp1[0] = 0 ; u_jp1[mx] = t[n]
            u_j[:] = u_jp1[:]

        return x,t,u_j,lmbda,deltax,deltat
        
    if type_of_BC=="neumann":
        #Setting ICs
        for i in range(0,mx+1):
            u_j[i] = u_I(x[i])  
        
        for n in range(1,mt+1):
            time=t[n]
            u_jp1=forwardmatrixneumann(mx+1,lmbda)@u_j + 2*lmbda*deltax*neumann(mx+1,-time**2,time)
            u_j[:]=u_jp1[:]

        return x,t,u_j,lmbda,deltax,deltat
    
     
       
#Plotting versus exact solution  
x,t,u_j,lmbda,dx,dt=finite_diff_forward(kappa,T,mx,mt,u_I,"dirichlet")
print("lambda =",lmbda)
plt.plot(x,u_j,'ro',label='num')    
xx = np.linspace(0,L,250)
plt.plot(xx,u_exact(xx,T),'b-',label='exact')
plt.xlabel('x')
plt.ylabel('u(x,T)')
plt.legend(loc='upper right')
plt.show()

#Animation code to show how solution changes with time

fig = plt.figure()
ax = plt.axes(xlim=(0, L), ylim=(0, 2))
line, = ax.plot([], [], lw=2)

def init():
    line.set_data([], [])
    return line,

def animate(i):
    x,t,u_j,lmbda,dx,dt=finite_diff_forward(kappa,T+0.01*i,mx,mt,u_I,"dirichlet")
    line.set_data(x,u_j)
    
    return line,
    
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=2000, interval=20, blit=True)

plt.show()

#Error analysis

errorx=[]
dxx=[]
dmx=np.arange(10,100,10)#changing the grid spacing in x
for n in dmx:
    x,t,u_j,lmbda,dx,dt=finite_diff_forward(kappa,T,n,200,u_I,"dirichlet")
    u_ex=u_exact(x,T)
    err=sum(np.abs(u_j-u_ex))/n
    errorx.append(err)
    dxx.append(dx)

plt.plot(dxx,errorx)
plt.xlabel("dx")
plt.ylabel("error")
plt.show()

errort=[]
dtt=[]
dmt=np.arange(10,100,10)#changing the grid sapcing in t
for n in dmt:
    x,t,u_j,lmbda,dx,dt=finite_diff_forward(kappa,T,50,n,u_I,"dirichlet")
    u_ex=u_exact(x,T)
    err=sum(np.abs(u_j-u_ex))/n
    errort.append(err)
    dtt.append(dt)

plt.plot(dtt,errort)
plt.xlabel("dt")
plt.ylabel("error")
plt.show()
    
    
