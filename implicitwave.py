###
# Week 11
# Question 1 part b

import numpy as np
from matplotlib import pyplot as plt
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve

# Set problem parameters/functions
c=1         # wavespeed
L=1.5         # length of spatial domain
T=1 # total time to solve for
def u_I(x):
    # initial displacement
    #y = 0*x
    y=np.sin(np.pi*x/L)
    return y

def v_I(x):
    # initial velocity
    y = 0*x
    #y = np.sin(np.pi*x/L)
    return y

def u_exact(x,t):
    # the exact solution
    y = np.cos(np.pi*c*t/L)*np.sin(np.pi*x/L)
    return y

def AIW(n,lmbda):
    """Using the implicit scheme returns the A matrix"""
    a=diags([-(lmbda**2)/2,1+lmbda**2,-(lmbda**2)/2],[-1,0,1],shape=(n,n),format = 'csr')
    return a
    
def BIW(n,lmbda):
    """Using the implicit scheme returns the B matrix"""
    b=diags([(lmbda**2)/2,-1-lmbda**2,(lmbda**2)/2],[-1,0,1],shape=(n,n),format = 'csr')
    return b
    
# Set numerical parameters
mx = 100 # number of gridpoints in space
mt = 50   # number of gridpoints in time

def wave_implicit(c,L,T,mx,mt):
    x = np.linspace(0, L, mx+1)     # gridpoints in space
    t = np.linspace(0, T, mt+1)     # gridpoints in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = c*deltat/deltax         # squared courant number
    print("lambda=",lmbda)

    u_jm1 = np.zeros(x.size)        # u at previous time step
    u_j = np.zeros(x.size)          # u at current time step
    u_jp1 = np.zeros(x.size)        # u at next time step

    # Set initial condition
    for i in range(0, mx+1):
        u_jm1[i] = u_I(x[i])    
    #First time step iteration
    for i in range(1,mx):
        u_j[1:mx]=spsolve(AIW(mx-1,lmbda)-BIW(mx-1,lmbda),2*u_jm1[1:mx]-2*deltat*v_I(x)[1:mx])
        u_j[0] = 0; u_j[mx]=0; #Setting BCs
    #Iteration scheme for rest of time steps 
    for n in range(2,mt+1):
        u_jp1[1:mx]=spsolve(AIW(mx-1,lmbda),2*u_j[1:mx] + BIW(mx-1,lmbda)@u_jm1[1:mx])
        u_jp1[0]=0;u_jp1[mx]=0 #Settting BCs
        u_jm1[:],u_j[:] = u_j[:],u_jp1[:]
    return x,u_jp1,lmbda

    
x,u_jp1,lmbda=wave_implicit(c,L,T,mx,mt)
plt.plot(x,u_jp1,'ro',label='numeric')
xx = np.linspace(0,L,250)
plt.plot(xx,u_exact(xx,T),'b-',label='exact')
plt.xlabel('x')
plt.ylabel('u(x,T)')
plt.legend()
plt.show()
        