###
# Week 11 solving hyperbolic PDEs
# Question 1 part a


import numpy as np
from matplotlib import pyplot as plt
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve


# Set problem parameters/functions
c=1.0         # wavespeed
L=14         # length of spatial domain
T=1     # total time to solve for

#Can change the initial conditons to find different types of waves

def u_I(x):
    # initial displacement
    y=np.cos(np.pi*x/L)
    return y

def v_I(x):
    # initial velocity
    y = 0*x
    return y

def u_exact(x,t):
    # the exact solution
    y = np.cos(np.pi*c*t/L)*np.cos(np.pi*x/L)
    return y

def AEW(n,lmbda):
    """Function to return normal forward digfference matrix"""
    a=diags([lmbda**2,2-2*lmbda**2,lmbda**2],[-1,0,1],shape=(n,n))
    return a

def forwardmatrixneumann(n,lmbda):
    """Function to return explicit forward difference matrix for Neumann BCs"""
    a=np.diag([lmbda**2]*(n-1),k=-1)
    b=np.diag([lmbda**2]*(n-1),k=1)    
    c=np.diag([2-2*lmbda**2]*n,k=0)
    d=a+b+c
    d[0,1]=2*lmbda**2
    d[n-1,n-2]=2*lmbda**2
    return d


# Set numerical parameters
mx = 70  # number of gridpoints in space
mt = 100     # number of gridpoints in time

def wave_explicit(c,L,T,mx,mt,type_of_bc):
    """Function to solve the wave equation using explicit finite difference,
    for 2 different types of types of BCs"""
    # Set up the numerical environment variables
    x = np.linspace(0, L, mx+1)     # gridpoints in space
    t = np.linspace(0, T, mt+1)     # gridpoints in time
    deltax = x[1] - x[0]            # gridspacing in x
    deltat = t[1] - t[0]            # gridspacing in t
    lmbda = c*deltat/deltax         # squared courant number
    #print("lambda=",lmbda)

    u_jm1 = np.zeros(x.size)        # u at previous time step
    u_j = np.zeros(x.size)          # u at current time step
    u_jp1 = np.zeros(x.size)        # u at next time step

    # Set initial condition
    for i in range(0, mx+1):
        u_jm1[i] = u_I(x[i])
        
    if type_of_bc=="dirichlet":
        #First time step
        for i in range(1,mx):
            u_j[1:mx]=0.5*AEW(mx-1,lmbda)@u_jm1[1:mx] + deltat*v_I(x)[1:mx]
            u_j[0] = 0; u_j[mx]=0;
        
        for n in range(2,mt+1):
            u_jp1[1:mx]=AEW(mx-1,lmbda)@u_j[1:mx] - u_jm1[1:mx]        
            u_jp1[0]=0;u_jp1[mx]=0 #Adding the BCs
            
            u_jm1[:],u_j[:] = u_j[:],u_jp1[:]
        return x,u_jp1,lmbda
        
    if type_of_bc=="neumann":
            
        #Different range due to neumann BCs
        for i in range(0,mx+1):
            u_j=0.5*forwardmatrixneumann(mx+1,lmbda)@u_jm1 +deltat*v_I(x[i])
            
        for n in range(2,mt+1):
            u_jp1=forwardmatrixneumann(mx+1,lmbda)@u_j - u_jm1
            
            u_jm1[:],u_j[:] = u_j[:],u_jp1[:]
        
        return x,u_jp1,lmbda


#Plotting versus exact solution 
x,u_jp1,lmbda=wave_explicit(c,L,T,mx,mt,"neumann")
plt.plot(x,u_jp1,'ro',label='numeric')
xx = np.linspace(0,L,250)
plt.plot(xx,u_exact(xx,T),'b-',label='exact')
plt.xlabel('x')
plt.ylabel('u(x,T)')
plt.legend()
plt.show()


